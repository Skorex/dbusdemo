# DbusDemo

🛠️ **Working on the D-Bus library and researching its proper usage.**

🌿 **Two branches exist to test with higher-level libraries:**
- `sd-bus`
- `pydbus`

⚠️ **Note:** As a demo of using these two libraries, this repository will no longer evolve.

🔍 **For debugging on Debian 12 to verify changes, the following tools are used:**
- `Bustle`
- `d-spy`